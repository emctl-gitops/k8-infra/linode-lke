# Remote Terraform state

# See https://docs.gitlab.com/ee/user/infrastructure/iac/terraform_state.html for details
# terraform init \
#    -backend-config="address=https://gitlab.com/api/v4/projects/$GITLAB_PROJECT_ID/terraform/state/$GITLAB_STATE_NAME" \
#    -backend-config="lock_address=https://gitlab.com/api/v4/projects/$GITLAB_PROJECT_ID/terraform/state/$GITLAB_STATE_NAME/lock" \
#    -backend-config="unlock_address=https://gitlab.com/api/v4/projects/$GITLAB_PROJECT_ID/terraform/state/$GITLAB_STATE_NAME/lock" \
#    -backend-config="username=$GITLAB_USERNAME" \
#    -backend-config="password=$GITLAB_PAT" \
#    -backend-config="lock_method=POST" \
#    -backend-config="unlock_method=DELETE" \
#    -backend-config="retry_wait_min=5"

#resource "kubernetes_namespace" "gitlab" {
#  metadata {
#    name = "gitlab-agent"
#  }
#}

#resource "kubernetes_secret" "gitlab_agent" {
#  metadata {
#    name      = "gitlab-agent-token-tf"
#    namespace = kubernetes_namespace.gitlab.metadata[0].name
#  }
#  data = {
#    token = "${var.gitlab_agent_token}"
#  }
#  type = "Opaque"
#}

resource "kubernetes_secret" "gitlab_runner" {
  metadata {
    name      = "gitlab-runner-token-tf"
    namespace = helm_release.gitlab_agent.metadata[0].namespace
  }
  data = {
    runner-registration-token = "${var.gitlab_runner_token}"
  }
  type = "Opaque"
}


#data "kubectl_path_documents" "docs" {
#  pattern = "${path.module}/agent-manifests/test.yaml"
#}

#resource "kubectl_manifest" "test" {
#  for_each           = toset(data.kubectl_path_documents.docs.documents)
#  yaml_body          = each.value
#  override_namespace = kubernetes_namespace.gitlab.metadata[0].name
#
#  depends_on = [
#    kubernetes_namespace.gitlab,
#  ]
# }

resource "helm_release" "gitlab_agent" {
  name                = "gitlab-agent"
  chart               = "https://gitlab.com/emctl-gitops/k8-infra/civo_testing/-/package_files/28864627/download"
  namespace           = "gitlab-agent"
  repository_username = var.cluster_bootstrap_username
  repository_password = var.gitlab_agent_token
  create_namespace    = true

  set {
    name  = "config.token"
    value = var.gitlab_agent_token
  }
}


# 
# 
# resource "kubernetes_service_account" "example" {
#   metadata {
#     name = "gitlab-agent"
#     namespace = "gitlab-agent"
#   }
# 
# }
# 
# resource "kubernetes_cluster_role_binding" "example" {
#   metadata {
#     name = "gitlab-agent-cluster-admin"
#   }
#   role_ref {
#     api_group = "rbac.authorization.k8s.io"
#     kind      = "ClusterRole"
#     name      = "cluster-admin"
#   }
#   subject {
#     kind      = "ServiceAccount"
#     name      = "gitlab-agent"
#     namespace =  "gitlab-agent"
# 
#   }
# 
# resource "kubernetes_deployment" "example" {
#   metadata = {
#     name = "gitlab-agent"
#     namespace = "gitlab-agent"
# 
#   }
# 
#   spec = {
#     replicas = 1
#     strategy = {  
#     type =  "RollingUpdate"
# 
#     rollingUpdate = {
#       maxSurge = 0
#       maxUnavailable = 1
#     }
#   }
#     selector = {
#       match_labels = {
#         test = "gitlab-agent"
#       }
#     }
# 
#     template = {
#       metadata = {
#         labels = {
#           app = "gitlab-agent"
#         }
#         annotations = {
#         "prometheus.io/path" = "/metrics"
#         "prometheus.io/port" = "8080"
#         "prometheus.io/scrape" =  "true"
#         }
#       }
# 
#       spec = {
# 
#         container = {
#           image = "registry.gitlab.com/gitlab-org/cluster-integration/gitlab-agent/agentk:stable"
#           name  = "agent"
#           args = [
#           "--token-file=/config/token",
#           "--kas-address",
#           "wss://kas.gitlab.com"
#           ]
# 
#           liveness_probe = {
#             http_get = {
#               path = "/liveness"
#               port = 8080
#             }
# 
#             initial_delay_seconds = 15
#             period_seconds        = 20
#           }
#           readinessProbe = {
#             http_get = {
#               path = "/readiness"
#               port = 8080
#             }
# 
#             initial_delay_seconds = 5
#             period_seconds        = 10
#           }
# 
#            volume_mount = {
#              mountpath = "/config"
#              name = "token-volume"
#            } 
# 
#         }
#         service_account_name = gitlab-agent
#         volume = {
#           name = "token-volume"
#           secret = {
#             secretName = gitlab-agent-token-tf
#           }
#         }
#       }
#     }
#   }
# }
# 
# 