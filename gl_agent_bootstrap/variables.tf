
variable "gitlab_agent_token" {
  description = "The token required to bootstrap the gitlab agent"
  type        = string
  sensitive   = true
}

variable "gitlab_runner_token" {
  description = "The token required to bootstrap the gitlab runner"
  type        = string
  sensitive   = true
}

variable "cluster_bootstrap_remote_state_address" {
  description = "The token required to bootstrap the gitlab agent"
  type        = string
}

variable "cluster_bootstrap_username" {
  description = "The token required to bootstrap the gitlab agent"
  type        = string
}

variable "cluster_bootstrap_access_token" {
  description = "The token required to bootstrap the gitlab agent"
  type        = string
  sensitive   = true
}
  