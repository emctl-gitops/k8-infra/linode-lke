# Specify required provider as maintained by civo
terraform {
  required_providers {
    #     civo = {
    #       source = "civo/civo"
    #     }
    kubectl = {
      source  = "gavinbunney/kubectl"
      version = "1.13.1"
    }
  }
  backend "http" {
    lock_method            = "POST"
    unlock_method          = "DELETE"
    retry_max              = 5
    skip_cert_verification = false
  }
}

# # Configure the Civo Provider
# provider "civo" {
#   region = "LON1"
#   token  = var.civo_token
# }

provider "kubectl" {
  host                   = "https://${data.terraform_remote_state.cluster_bootstrap.outputs.kubernetes_cluster_hostname}:6443"
  load_config_file       = false
  client_key             = base64decode(yamldecode(data.terraform_remote_state.cluster_bootstrap.outputs.kubernetes_cluster_output).users.0.user.client-key-data)
  client_certificate     = base64decode(yamldecode(data.terraform_remote_state.cluster_bootstrap.outputs.kubernetes_cluster_output).users.0.user.client-certificate-data)
  cluster_ca_certificate = base64decode(yamldecode(data.terraform_remote_state.cluster_bootstrap.outputs.kubernetes_cluster_output).clusters.0.cluster.certificate-authority-data)
}
provider "kubernetes" {
  host                   = "https://${data.terraform_remote_state.cluster_bootstrap.outputs.kubernetes_cluster_hostname}:6443"
  client_key             = base64decode(yamldecode(data.terraform_remote_state.cluster_bootstrap.outputs.kubernetes_cluster_output).users.0.user.client-key-data)
  client_certificate     = base64decode(yamldecode(data.terraform_remote_state.cluster_bootstrap.outputs.kubernetes_cluster_output).users.0.user.client-certificate-data)
  cluster_ca_certificate = base64decode(yamldecode(data.terraform_remote_state.cluster_bootstrap.outputs.kubernetes_cluster_output).clusters.0.cluster.certificate-authority-data)
}

provider "helm" {
  kubernetes {
    host                   = "https://${data.terraform_remote_state.cluster_bootstrap.outputs.kubernetes_cluster_hostname}:6443"
    client_key             = base64decode(yamldecode(data.terraform_remote_state.cluster_bootstrap.outputs.kubernetes_cluster_output).users.0.user.client-key-data)
    client_certificate     = base64decode(yamldecode(data.terraform_remote_state.cluster_bootstrap.outputs.kubernetes_cluster_output).users.0.user.client-certificate-data)
    cluster_ca_certificate = base64decode(yamldecode(data.terraform_remote_state.cluster_bootstrap.outputs.kubernetes_cluster_output).clusters.0.cluster.certificate-authority-data)
  }
}
