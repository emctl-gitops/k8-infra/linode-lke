## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_kubectl"></a> [kubectl](#requirement\_kubectl) | 1.13.1 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_kubectl"></a> [kubectl](#provider\_kubectl) | 1.13.1 |
| <a name="provider_kubernetes"></a> [kubernetes](#provider\_kubernetes) | 2.7.1 |
| <a name="provider_terraform"></a> [terraform](#provider\_terraform) | n/a |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [kubectl_manifest.test](https://registry.terraform.io/providers/gavinbunney/kubectl/1.13.1/docs/resources/manifest) | resource |
| [kubernetes_namespace.gitlab](https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs/resources/namespace) | resource |
| [kubernetes_secret.gitlab_agent](https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs/resources/secret) | resource |
| [kubernetes_secret.gitlab_runner](https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs/resources/secret) | resource |
| [kubectl_path_documents.docs](https://registry.terraform.io/providers/gavinbunney/kubectl/1.13.1/docs/data-sources/path_documents) | data source |
| [terraform_remote_state.cluster_bootstrap](https://registry.terraform.io/providers/hashicorp/terraform/latest/docs/data-sources/remote_state) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_cluster_bootstrap_access_token"></a> [cluster\_bootstrap\_access\_token](#input\_cluster\_bootstrap\_access\_token) | The token required to bootstrap the gitlab agent | `string` | n/a | yes |
| <a name="input_cluster_bootstrap_remote_state_address"></a> [cluster\_bootstrap\_remote\_state\_address](#input\_cluster\_bootstrap\_remote\_state\_address) | The token required to bootstrap the gitlab agent | `string` | n/a | yes |
| <a name="input_cluster_bootstrap_username"></a> [cluster\_bootstrap\_username](#input\_cluster\_bootstrap\_username) | The token required to bootstrap the gitlab agent | `string` | n/a | yes |
| <a name="input_gitlab_agent_token"></a> [gitlab\_agent\_token](#input\_gitlab\_agent\_token) | The token required to bootstrap the gitlab agent | `string` | n/a | yes |
| <a name="input_gitlab_runner_token"></a> [gitlab\_runner\_token](#input\_gitlab\_runner\_token) | The token required to bootstrap the gitlab runner | `string` | n/a | yes |

## Outputs

No outputs.
