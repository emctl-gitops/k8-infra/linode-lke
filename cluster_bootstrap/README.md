### How to get kubeconfig
```
rm local.kubeconfig
kubectl config unset clusters.my-cluster
terraform output -json | jq -r .kubernetes_cluster_output.value > local.kubeconfig
export PWD_CONFIG=`pwd`
export KUBECONFIG=$PWD_CONFIG/local.kubeconfig:~/.kube/config
kubectl config set-context my-cluster
kubectl get ns;
```

<!-- BEGIN_TF_DOCS -->
## Requirements

No requirements.

## Providers

| Name | Version |
|------|---------|
| <a name="provider_linode"></a> [linode](#provider\_linode) | 1.28.1 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [linode_lke_cluster.my-cluster](https://registry.terraform.io/providers/linode/linode/latest/docs/resources/lke_cluster) | resource |

## Inputs

No inputs.

## Outputs

No outputs.
