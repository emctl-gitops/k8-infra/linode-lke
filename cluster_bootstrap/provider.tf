# Specify required provider as maintained by civo
terraform {
  required_providers {
    linode = {
      source = "linode/linode"
    }

  }
  # backend "http" {
  # }
}

# # Configure the Civo Provider
# provider "civo" {
#   region = "LON1"
#   token  = var.civo_token
# }
